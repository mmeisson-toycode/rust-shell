
pub mod tokenizer {

	static ESCAPE_CHARS: &str = "\"'`";
	static SPECIAL_CHARS: &str = "&|";

	#[derive(PartialEq, Debug)]
	enum TokenState {
		Space,
		Word,
		EscapeSeq,
		Token,
	}

	#[derive(Debug)]
	pub struct Tokenizer<'a> {
		input: &'a String,
		index: usize,
		token_start: usize,
		char_start: char,
		state: TokenState,
		escaped: bool,
	}

	fn get_state(c: char) -> TokenState {
		if c.is_whitespace() {
			return TokenState::Space;
		}
		if ESCAPE_CHARS.contains(c) {
			return TokenState::EscapeSeq;
		}
		return TokenState::Word;
	}

	impl<'a> Tokenizer<'a> {

		pub fn new(input: &'a String) -> Tokenizer<'a> {
			Tokenizer {
				input: input,
				index: 0,
				token_start: 0,
				char_start: 0 as char,
				state: TokenState::Space,
				escaped: false,
			}
		}
	}

	impl<'a> Iterator for Tokenizer<'a> {

		type Item = &'a str;

		fn next(&mut self) -> Option<&'a str> {
			let it = self.input.chars();

			for (i, c) in it.enumerate().skip(self.index) {
				let letter_state = get_state(c);

				match self.state {
					TokenState::Space => {

						match letter_state {
							TokenState::Space => {},
							_ => {
								self.token_start = i;
								self.char_start = c;
								if c == '\\' {
									self.escaped = true;
								}
								self.state = letter_state;
								return Some("")
							}
						}
					},
					_ => {
						if self.escaped {
							self.escaped = false;
							continue ;
						}

						if c == '\\' {
							self.escaped = true;
						}
						else if letter_state == TokenState::Space
							&& self.state == TokenState::Word {
							self.index = i + 1;
							let token: & str = &self.input[self.token_start..i - self.token_start];

							self.state = TokenState::Space;
							return Some(token);
						}
						else if letter_state == TokenState::EscapeSeq {

							/* We have an escape char like ' or "
							** if we are in escape state, we look if the character correspond to the closing we are waiting for
							** if we are in word state, then we have a new token
							*/
							println!("{} {:?}", self.input.chars().nth(self.token_start).unwrap(), self);
							if self.state == TokenState::Word ||
								c == self.char_start {
								self.index = i + 1;
								let token: & str = &self.input[self.token_start..i - self.token_start];

								self.token_start = i + 1;

								self.state = letter_state;
								return Some(token)
							}
						}
					},
				}
			}
			return None;
		}
	}

}