
#[macro_use]
extern crate clap;
mod tokenizer;
mod input;
mod prompt;

use tokenizer::tokenizer::Tokenizer;
use std::io::{BufRead, self};
use input::input::Input;

fn main() {
	let mut command_line: String = String::new();
	let yaml = load_yaml!("cli.yaml");
	let matches = clap::App::from_yaml(yaml).get_matches();

	let input: Result<Input, _> = match matches.value_of("file") {
			Some(file_name) => {
				Input::file(&file_name)
			},
			None => {
				match matches.value_of("command") {
					Some(cl) => {
						command_line = cl.to_string();
						Ok( Input::string(&command_line) )
					},
					None => {
						Ok( Input::console() )
					},
				}
			},
		};

	input.unwrap().lines()
		.filter(|input| input.is_ok())
		.map(|input| input.unwrap())
		.filter(|input| input.len() > 0)
		.map(|input| -> () {
			let t = Tokenizer::new(&input);

			t.for_each(|input| println!("{:?}", input));
		}).for_each(|_| {});

}
