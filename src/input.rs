
pub mod input {

	extern crate stringreader;

	use prompt::prompt::PromptShell;
	use std::fs::File;
	use std::io::{self, BufRead, BufReader, Read};
	use self::stringreader::StringReader;


	pub struct Input<'a> {
		source: Box<BufRead + 'a>,
	}

	impl<'a> Input<'a> {
		pub fn console() -> Input<'a> {
			Input {
				source: Box::new(PromptShell::new())
			}
		}

		pub fn file(path: &str) -> io::Result<Input<'a>> {
			File::open(path).map(|file| Input {
				source: Box::new(io::BufReader::new(file)),
			})
		}

		pub fn string(stream: &'a String) -> Input<'a> {
			Input {
				source: Box::new(BufReader::new(StringReader::new(stream))),
			}
		}
	}

	impl<'a> Read for Input<'a> {
		fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
			self.source.read(buf)
		}
	}

	impl<'a> BufRead for Input<'a> {
		fn fill_buf(&mut self) -> io::Result<&[u8]> {
			self.source.fill_buf()
		}

		fn consume(&mut self, amt: usize) {
			self.source.consume(amt);
		}
	}

}