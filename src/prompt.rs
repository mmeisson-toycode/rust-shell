
pub mod prompt {

	use std::io::{self, BufRead, BufReader, Read, Stdin, Write, stdout};

	pub struct PromptShell<'a> {
		prompt: String,
		line: Box<BufRead + 'a>,
	}

	impl <'a>PromptShell<'a> {

		pub fn new() -> PromptShell<'a> {
			PromptShell {
				prompt: "$> ".to_string(),
				line: Box::new(BufReader::new(io::stdin())),
			}
		}

		pub fn set_prompt(&mut self, prompt: &str) {
			self.prompt = prompt.to_string();
		}

	}

	impl<'a> Read for PromptShell<'a> {
		fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
			self.line.read(buf)
		}
	}

	#[allow(unused_must_use)]
	impl<'a> BufRead for PromptShell<'a> {
		fn fill_buf(&mut self) -> io::Result<&[u8]> {

			print!("{}", self.prompt);
			stdout().flush();
			self.line.fill_buf()
		}

		fn consume(&mut self, amt: usize) {
			self.line.consume(amt);
		}
	}

}